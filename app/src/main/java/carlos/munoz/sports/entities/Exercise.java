package carlos.munoz.sports.entities;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import carlos.munoz.sports.R;

@ParseClassName("exercise")
public class Exercise extends ParseObject {

    public static final String FIELD_EX_SPORT = "sport";
    public static final String FIELD_EX_DESCRIPTION = "description";
    public static final String FIELD_EX_TITLE = "title";
    public static final String FIELD_EX_LEVEL = "level";
    public static final String FIELD_EX_CREATOR = "creator";
    public static final String FIELD_EX_LOCATION = "location";

    public static final String FIELD_EX_DATE_START = "date_start";
    public static final String FIELD_EX_DATE_END = "date_end";
    public static final String FIELD_EX_WEEKDAYS = "weekdays";

    public Exercise() {
        // A default constructor is required.
    }

    public int getSport() {
        return getInt(FIELD_EX_SPORT);
    }

    /**
     * @param context needed to get string resources
     */
    public String getPrintableSport(Context context) {
        String[] sportNames = context.getResources().getStringArray(R.array.sport_titles);
        return sportNames[getInt(FIELD_EX_SPORT)];
    }

    public void setSport(int sport) {
        put(FIELD_EX_SPORT, sport);
    }

    public ParseUser getCreator() {
        return getParseUser(FIELD_EX_CREATOR);
    }

    public void setCreator(ParseUser user) {
        put(FIELD_EX_CREATOR, user);
    }

    public String getTitle() {
        return getString(FIELD_EX_TITLE);
    }

    public void setTitle(String title) {
        put(FIELD_EX_TITLE, title);
    }

    public String getDescription() {
        return getString(FIELD_EX_DESCRIPTION);
    }

    public void setDescription(String description) {
        put(FIELD_EX_DESCRIPTION, description);
    }

    public int getLevel() {
        return getInt(FIELD_EX_LEVEL);
    }

    public void setLevel(int level) {
        put(FIELD_EX_LEVEL, level);
    }

    public void setLocation(double lat, double lon) {
        put(FIELD_EX_LOCATION, new ParseGeoPoint(lat, lon));
    }

    public LatLng getLocation() {
        ParseGeoPoint location = getParseGeoPoint(FIELD_EX_LOCATION);
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public Date getStartDate() {
        return getDate(FIELD_EX_DATE_START);
    }

    public void setStartDate(Date startDate) {
        put(FIELD_EX_DATE_START, startDate);
    }

    public Date getEndDate() {
        return getDate(FIELD_EX_DATE_END);
    }

    public void setEndDate(Date endDate) {
        put(FIELD_EX_DATE_END, endDate);
    }

    public void setWeekDays(ArrayList<String> weekDays) {
        remove(FIELD_EX_WEEKDAYS);
        addAll(FIELD_EX_WEEKDAYS, weekDays);
    }

    public ArrayList<String> getWeekDays() {
        return (ArrayList<String>) get(FIELD_EX_WEEKDAYS);
    }

    /**
     * builds human readable string with weekdays, start and end dates
     * @return human readable full date.
     */
    public String getPrintableFullDate(){
        StringBuilder sb = new StringBuilder();

        if (getStartDate() != null && getStartDate().getTime() != 100000000
                && getEndDate() != null && getEndDate().getTime() != 100000000
                && getWeekDays() != null && getWeekDays().size() > 0) {

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

            for (int i = 0; i < getWeekDays().size(); i++) {
                sb.append(getWeekDays().get(i));
                if (i == getWeekDays().size() - 2) {
                    sb.append("s and ");
                } else {
                    sb.append("s, ");
                }
            }
            sb.append(" from ");
            sb.append(sdf.format(getStartDate()));
            sb.append(" to ");
            sb.append(sdf.format(getEndDate()));
        } else {
            sb.append("Select times and weekdays");
        }
        return sb.toString();
    }
}