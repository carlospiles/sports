package carlos.munoz.sports.entities;

public class Sport {

    //reference stored in cloud
    private int code;
    private String name;
    private int imgResource;

    public Sport(int code, String name, int imgResource) {
        this.code = code;
        this.name = name;
        this.imgResource = imgResource;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }
}
