package carlos.munoz.sports.entities;

import com.orm.SugarRecord;

public class Message extends SugarRecord<Message> {

    String roomId;
    String userName;
    String userId;
    String messageContent;
    String messageId;

    public Message() {
    }

    public Message(String roomId, String userName, String userId, String messageContent, String messageId) {
        this.roomId = roomId;
        this.userName = userName;
        this.messageContent = messageContent;
        this.userId = userId;
        this.messageId = messageId;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public String getMessageId() {
        return messageId;
    }
}