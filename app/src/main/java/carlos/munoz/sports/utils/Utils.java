package carlos.munoz.sports.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.MatrixCursor;
import android.location.Address;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.model.GeocodingResult;

import java.util.ArrayList;

import carlos.munoz.sports.R;
import carlos.munoz.sports.entities.Sport;

public class Utils {
    public static final String LOG_DEBUG_TAG = "SPORTS_DEBUG";

    private static final double ASSUMED_INIT_LATLNG_DIFF = 1.0;
    private static final float ACCURACY = 0.01f;

    /**
     * Transforms circle coordinates into bounds for camera movement
     *
     * @param center
     * @param latDistanceInMeters
     * @param lngDistanceInMeters
     * @return
     */
    public static LatLngBounds boundsWithCenterAndLatLngDistance(
            LatLng center, float latDistanceInMeters, float lngDistanceInMeters) {

        latDistanceInMeters /= 2;
        lngDistanceInMeters /= 2;
        LatLngBounds.Builder builder = LatLngBounds.builder();
        float[] distance = new float[1];
        {
            boolean foundMax = false;
            double foundMinLngDiff = 0;
            double assumedLngDiff = ASSUMED_INIT_LATLNG_DIFF;
            do {
                Location.distanceBetween(center.latitude, center.longitude,
                        center.latitude, center.longitude + assumedLngDiff,
                        distance);
                float distanceDiff = distance[0] - lngDistanceInMeters;
                if (distanceDiff < 0) {
                    if (!foundMax) {
                        foundMinLngDiff = assumedLngDiff;
                        assumedLngDiff *= 2;
                    } else {
                        double tmp = assumedLngDiff;
                        assumedLngDiff += (assumedLngDiff - foundMinLngDiff) / 2;
                        foundMinLngDiff = tmp;
                    }
                } else {
                    assumedLngDiff -= (assumedLngDiff - foundMinLngDiff) / 2;
                    foundMax = true;
                }
            } while (Math.abs(distance[0] - lngDistanceInMeters) > lngDistanceInMeters
                    * ACCURACY);
            LatLng east = new LatLng(center.latitude, center.longitude
                    + assumedLngDiff);
            builder.include(east);
            LatLng west = new LatLng(center.latitude, center.longitude
                    - assumedLngDiff);
            builder.include(west);
        }
        {
            boolean foundMax = false;
            double foundMinLatDiff = 0;
            double assumedLatDiffNorth = ASSUMED_INIT_LATLNG_DIFF;
            do {
                Location.distanceBetween(center.latitude, center.longitude,
                        center.latitude + assumedLatDiffNorth,
                        center.longitude, distance);
                float distanceDiff = distance[0] - latDistanceInMeters;
                if (distanceDiff < 0) {
                    if (!foundMax) {
                        foundMinLatDiff = assumedLatDiffNorth;
                        assumedLatDiffNorth *= 2;
                    } else {
                        double tmp = assumedLatDiffNorth;
                        assumedLatDiffNorth += (assumedLatDiffNorth - foundMinLatDiff) / 2;
                        foundMinLatDiff = tmp;
                    }
                } else {
                    assumedLatDiffNorth -= (assumedLatDiffNorth - foundMinLatDiff) / 2;
                    foundMax = true;
                }
            } while (Math.abs(distance[0] - latDistanceInMeters) > latDistanceInMeters
                    * ACCURACY);
            LatLng north = new LatLng(center.latitude + assumedLatDiffNorth,
                    center.longitude);
            builder.include(north);
        }
        {
            boolean foundMax = false;
            double foundMinLatDiff = 0;
            double assumedLatDiffSouth = ASSUMED_INIT_LATLNG_DIFF;
            do {
                Location.distanceBetween(center.latitude, center.longitude,
                        center.latitude - assumedLatDiffSouth,
                        center.longitude, distance);
                float distanceDiff = distance[0] - latDistanceInMeters;
                if (distanceDiff < 0) {
                    if (!foundMax) {
                        foundMinLatDiff = assumedLatDiffSouth;
                        assumedLatDiffSouth *= 2;
                    } else {
                        double tmp = assumedLatDiffSouth;
                        assumedLatDiffSouth += (assumedLatDiffSouth - foundMinLatDiff) / 2;
                        foundMinLatDiff = tmp;
                    }
                } else {
                    assumedLatDiffSouth -= (assumedLatDiffSouth - foundMinLatDiff) / 2;
                    foundMax = true;
                }
            } while (Math.abs(distance[0] - latDistanceInMeters) > latDistanceInMeters
                    * ACCURACY);
            LatLng south = new LatLng(center.latitude - assumedLatDiffSouth,
                    center.longitude);
            builder.include(south);
        }
        return builder.build();
    }

    /**
     * build human readable address from Address object
     *
     * @param address address to parse
     * @return human readable address
     */
    public static String getPrintableAddress(Address address) {
        String addressText = "";
        addressText += address.getMaxAddressLineIndex() > 0 ? address
                .getAddressLine(0) + ", " : "";
        addressText += address.getLocality() != null ? address.getLocality()
                + ", " : "";
        addressText += address.getCountryName() != null ? address
                .getCountryName() + ", " : "";
        if (addressText.length() > 0) {
            return addressText.substring(0, addressText.length() - 2);
        } else {
            return addressText;
        }

    }

    /**
     * build cursor from addresses list. Needed for suggestions cursorAdapter
     *
     * @param addresses addresses to convert into cursor
     * @return cursor with addresses
     */
    public static MatrixCursor generateCursor(GeocodingResult[] addresses) {
        // Cursor
        String[] columns = new String[]{"_id", "text"};
        Object[] temp = new Object[]{0, "default"};

        MatrixCursor cursor = new MatrixCursor(columns);

        for (int i = 0; i < addresses.length; i++) {

            temp[0] = i;
            temp[1] = addresses[i].formattedAddress;

            cursor.addRow(temp);
        }
        return cursor;
    }

    /**
     * build Sport list from resources
     *
     * @param context needed to get resources
     * @return sport list (title, image and id)
     */
    public static ArrayList<Sport> generateSportData(Context context) {
        ArrayList<Sport> sports = new ArrayList<>();
        String[] sport_titles = context.getResources().getStringArray(R.array.sport_titles);
        TypedArray sport_images = context.getResources().obtainTypedArray(R.array.sport_icons);
        for (int i = 0; i < sport_titles.length; i++) {
            sports.add(new Sport(i, sport_titles[i], sport_images.getResourceId(i, -1)));
        }
        return sports;
    }

    /**
     * Build facebook picture url based on facebook id
     * @param fbId
     * @return
     */
    public static String buildFbPictureUrl(String fbId) {
        return "https://graph.facebook.com/" + fbId + "/picture?type=large";
    }
}
