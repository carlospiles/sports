package carlos.munoz.sports.common;

import android.util.Log;

import com.orm.SugarApp;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import carlos.munoz.sports.entities.Exercise;

public class SportsApplication extends SugarApp {

    private static SportsApplication instance;

    public static SportsApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        // Add your initialization code here
        Parse.initialize(this, "E7RkvBMkln0dmSQpcIfk8sljs0dhr123uiRdZhul",
                "7Ij66ypVI3R4iStvatS7RfyrWlrkBAZpmVcZPchz");

        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

        // Register subclasses
        ParseObject.registerSubclass(Exercise.class);

        // ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }
}
