package carlos.munoz.sports.common;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.ui.account.LoginActivity;
import carlos.munoz.sports.ui.account.MyAccountActivity;
import carlos.munoz.sports.ui.home.HomeActivity;
import carlos.munoz.sports.ui.newexercise.NewExerciseActivity;
import carlos.munoz.sports.ui.viewexercise.ViewExerciseActivity;

public class Navigator {

    private static Navigator mInstance = null;

    public static final String INTENT_EXTRA_EXERCISE_ID = "exercise_id";

    private Navigator() {
    }

    public static Navigator getInstance() {
        if (mInstance == null) {
            mInstance = new Navigator();
        }
        return mInstance;
    }

    //Navigator classes here ...

    public void goToAccount(Context context, boolean logged) {
        Intent i;
        if (logged){
            i = new Intent(context, MyAccountActivity.class);
        }else{
            i = new Intent(context, LoginActivity.class);
        }
        context.startActivity(i);
    }


    public void goToMain(Context context) {
        Intent i = new Intent(context, HomeActivity.class);
        context.startActivity(i);
    }

    public void goToNewExercise(Context context) {
        Intent i = new Intent(context, NewExerciseActivity.class);
        context.startActivity(i);
    }

    public void goToViewExercise(Exercise exercise, Context context) {
        Intent i = new Intent(context, ViewExerciseActivity.class);
        i.putExtra(INTENT_EXTRA_EXERCISE_ID, exercise.getObjectId());
        context.startActivity(i);
    }

    public void reloadActivity(Activity activity){
        activity.finish();
        activity.startActivity(activity.getIntent());
    }
}
