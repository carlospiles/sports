package carlos.munoz.sports.common;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import carlos.munoz.sports.R;

public abstract class BaseActivity extends ActionBarActivity implements BaseUi {

    private Toolbar toolbar;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected abstract int getLayoutResource();

    /*
    ACTIONBAR SETUP
     */

    protected void setActionBarIcon(int iconRes) {
        toolbar.setNavigationIcon(iconRes);
    }

    protected void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    protected void setActionBarNavigationClickListener(View.OnClickListener cl) {
        toolbar.setNavigationOnClickListener(cl);
    }

    /*
    TOAST AND PROGRESS DIALOGS
     */

    @Override
    public void showProgressDialog(String title, String text) {
        mProgressDialog = ProgressDialog.show(this, title,
                text, true);
    }

    @Override
    public void hideProgressDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showTutorial(String title, String text, int shotID, View view) {

        //Align close tutorial button to bottom left
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
        lps.setMargins(margin, margin, margin, margin);

        //create showcaseview
        ShowcaseView sv = new ShowcaseView.Builder(this)
                .setContentTitle(title)
                .setContentText(text)
                .singleShot(shotID)
                .setTarget(new ViewTarget(view))
                .setStyle(R.style.CustomShowcaseTheme)
                .hideOnTouchOutside()
                .build();

        sv.setButtonPosition(lps);
    }
}
