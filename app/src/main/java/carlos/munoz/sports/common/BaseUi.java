package carlos.munoz.sports.common;

import android.view.View;

public interface BaseUi {

    public final int TUTORIAL_MAP = 10;
    public final int TUTORIAL_FILTERS = 11;
    public final int TUTORIAL_NEW_EXERCISE = 12;
    public final int TUTORIAL_VIEW_EXERCISE = 13;

    public void showProgressDialog(String title, String text);

    public void hideProgressDialog();

    public void showToast(String msg);

    public void showTutorial(String title, String text, int shotID, View view);
}
