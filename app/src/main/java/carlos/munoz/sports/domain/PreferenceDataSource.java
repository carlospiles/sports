package carlos.munoz.sports.domain;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import carlos.munoz.sports.entities.Exercise;

public class PreferenceDataSource {

    public static final String PREF_START_DATE = "PREF_START_DATE";
    public static final String PREF_END_DATE = "PREF_END_DATE";
    public static final String PREF_LEVEL = "PREF_LEVEL";
    public static final String PREF_SPORT = "PREF_SPORT";
    public static final String PREF_WEEKDAYS = "PREF_WEEKDAYS";
    public static final String PREF_RADIUS = "PREF_RADIUS";
    public static final String PREF_FILTER_CHANGED = "PREF_FILTER_CHANGED";

    public static final String PREFERENCES_NAME = "MY_PREFERENCES";

    Context context;
    SharedPreferences prefs;

    public PreferenceDataSource(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public int getRadius() {
        return prefs.getInt(PREF_RADIUS, 100);
    }

    public void setRadius(int radius) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREF_RADIUS, radius);
        editor.apply();
    }

    public Exercise readFilterExercise() {
        Exercise filterExercise = new Exercise();

        filterExercise.setStartDate(new Date(prefs.getLong(PREF_START_DATE, 100000000)));
        filterExercise.setEndDate(new Date(prefs.getLong(PREF_END_DATE, 100000000)));
        filterExercise.setLevel(prefs.getInt(PREF_LEVEL, -1));
        filterExercise.setSport(prefs.getInt(PREF_SPORT, -1));
        filterExercise.setWeekDays(new ArrayList<>(prefs
                .getStringSet(PREF_WEEKDAYS, new HashSet<String>())));

        return filterExercise;
    }

    public void writeFilterExercise(Exercise filterExercise) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(PREF_START_DATE, filterExercise.getStartDate().getTime());
        editor.putLong(PREF_END_DATE, filterExercise.getEndDate().getTime());
        editor.putInt(PREF_LEVEL, filterExercise.getLevel());
        editor.putInt(PREF_SPORT, filterExercise.getSport());
        editor.putStringSet(PREF_WEEKDAYS, new HashSet<>(filterExercise.getWeekDays()));

        editor.apply();
    }

    public void writeFilterChanged(boolean filterExerciseChanged) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_FILTER_CHANGED, filterExerciseChanged);
        editor.apply();
    }

    public boolean readFilterChanged() {
        return prefs.getBoolean(PREF_FILTER_CHANGED, false);
    }
}