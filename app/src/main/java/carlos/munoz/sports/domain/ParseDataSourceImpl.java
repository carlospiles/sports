package carlos.munoz.sports.domain;

import android.app.Activity;
import android.util.Log;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.google.android.gms.maps.model.LatLng;
import com.parse.CountCallback;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.listener.FacebookLoginListener;
import carlos.munoz.sports.utils.Utils;

public class ParseDataSourceImpl implements ParseDataSource {

    public static final String FIELD_USER_JOINED_EXERCISES = "joined_exercises";

    @Override
    public void signUp(String name, String email, String pass, SignUpCallback callback) {

        ParseUser user = new ParseUser();
        user.setUsername(name);
        user.setPassword(pass);
        user.setEmail(email);
        user.signUpInBackground(callback);
    }

    @Override
    public void logIn(String name, String pass, LogInCallback callback) {
        ParseUser.logInInBackground(name, pass, callback);
    }

    @Override
    public void facebookLogIn(Activity returnActivity, final FacebookLoginListener callback) {
        ParseFacebookUtils.logIn(
                Arrays.asList(
                        ParseFacebookUtils.Permissions.User.ABOUT_ME,
                        ParseFacebookUtils.Permissions.User.EMAIL),
                returnActivity,
                new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if (e == null) {
                            if (parseUser.isNew()) {
                                Log.e(Utils.LOG_DEBUG_TAG, "new parse user");
                                getFacebookIdInBackground(callback);
                            } else {
                                callback.onSuccess();
                            }
                        } else {
                            callback.onError(e.getMessage());
                            Log.e(Utils.LOG_DEBUG_TAG, "ERROR 0: " + e.getMessage());
                        }
                    }
                });
    }

    /**
     * Save extra facebook info into Parse via Graph query.
     *
     * @param callback called when Graph query completed.
     */
    private void getFacebookIdInBackground(final FacebookLoginListener callback) {
        Request.newMeRequest(ParseFacebookUtils.getSession(), new Request.GraphUserCallback() {
            @Override
            public void onCompleted(final GraphUser user, Response response) {
                if (user != null) {

                    ParseUser.getCurrentUser().put("fbId", user.getId());
                    //generate "private name (shorted last name)"
                    String privateName = user.getProperty("first_name")
                            + " " + user.getProperty("last_name").toString().substring(0, 1) + ".";

                    ParseUser.getCurrentUser().setUsername(privateName);
                    ParseUser.getCurrentUser().setEmail((String) user.getProperty("email"));

                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.e(Utils.LOG_DEBUG_TAG, "LOGEADO: " + user.getName());
                                callback.onSuccess();
                            } else {
                                callback.onError(e.getMessage());
                                Log.e(Utils.LOG_DEBUG_TAG, "ERROR 1: " + e.getMessage());
                            }
                        }
                    });
                } else {
                    callback.onError("error requesting user info to facebook");
                    Log.e(Utils.LOG_DEBUG_TAG, "ERROR 2: error requesting user info to facebook");
                }
            }
        }).executeAsync();
    }

    @Override
    public void logOut() {
        ParseUser.logOut();
    }

    @Override
    public void joinExercise(ParseUser participant, ParseObject exercise, SaveCallback callback) {
        ParseRelation<ParseObject> joinedExercises =
                participant.getRelation(FIELD_USER_JOINED_EXERCISES);
        joinedExercises.add(exercise);
        participant.saveInBackground(callback);
    }

    @Override
    public void leaveExercise(ParseUser participant, ParseObject exercise, SaveCallback callback) {
        ParseRelation<ParseObject> joinedExercises =
                participant.getRelation(FIELD_USER_JOINED_EXERCISES);
        joinedExercises.remove(exercise);
        participant.saveInBackground(callback);
    }

    @Override
    public void findUserById(String parseUserId, GetCallback<ParseUser> callback) {
        ParseQuery<ParseUser> query = ParseQuery.getQuery(ParseUser.class);
        query.getInBackground(parseUserId, callback);
    }

    @Override
    public void checkIfUserJoined(ParseUser user, ParseObject exercise, CountCallback callback) {
        ParseQuery<ParseObject> query =  user.getRelation(FIELD_USER_JOINED_EXERCISES).getQuery();
        query.whereEqualTo("objectId", exercise.getObjectId());
        query.countInBackground(callback);
    }

    @Override
    public void createExercise(int category, int level, String title,
                               String desc, ParseUser user,
                               double lat, double lon, Date startDate,
                               Date endDate, ArrayList<String> weekdays, SaveCallback callback) {

        Exercise exercise = new Exercise();

        exercise.setSport(category);
        exercise.setLevel(level);
        exercise.setTitle(title);
        exercise.setDescription(desc);
        exercise.setCreator(user);
        exercise.setLocation(lat, lon);
        exercise.setStartDate(startDate);
        exercise.setEndDate(endDate);
        exercise.setWeekDays(weekdays);

        exercise.saveInBackground(callback);
    }

    @Override
    public void deleteExercise(Exercise exercise, DeleteCallback callback) {
        exercise.deleteInBackground(callback);
    }


    @Override
    public void findExercises(LatLng targetLocation, double radiusInKms,
                              Exercise filterExercise, FindCallback<Exercise> callback) {

        StringBuilder sb = new StringBuilder();

        ParseGeoPoint targetGeoPoint = new ParseGeoPoint(targetLocation.latitude,
                targetLocation.longitude);
        ParseQuery<Exercise> query = ParseQuery.getQuery(Exercise.class);

        sb.append("SEARCHING: \n");

        query.whereWithinKilometers(Exercise.FIELD_EX_LOCATION, targetGeoPoint, radiusInKms);

        sb.append("target: ").append(targetGeoPoint.toString()).append("\n");
        sb.append("radius in kms: ").append(radiusInKms).append("\n");

        if (!filterExercise.getWeekDays().isEmpty()) {
            query.whereContainedIn(Exercise.FIELD_EX_WEEKDAYS, filterExercise.getWeekDays());
            sb.append("filter weekdays: ").append(filterExercise.toString()).append("\n");
        }
        if (filterExercise.getStartDate().getTime() != 100000000) {
            query.whereGreaterThanOrEqualTo(Exercise.FIELD_EX_DATE_START, filterExercise.getStartDate());
            sb.append("filter start date: ").append(filterExercise.getStartDate().toString()).append("\n");

        }
        if (filterExercise.getEndDate().getTime() != 100000000) {
            query.whereLessThanOrEqualTo(Exercise.FIELD_EX_DATE_END, filterExercise.getEndDate());
            sb.append("filter end date: ").append(filterExercise.getEndDate().toString()).append("\n");

        }
        if (filterExercise.getLevel() > 0) {
            query.whereEqualTo(Exercise.FIELD_EX_LEVEL, filterExercise.getLevel());
            sb.append("filter level: ").append(filterExercise.getLevel()).append("\n");
        }
        if (filterExercise.getSport() >= 0) {
            query.whereEqualTo(Exercise.FIELD_EX_SPORT, filterExercise.getSport());
            sb.append("filter sport: ").append(filterExercise.getSport()).append("\n");
        }

        Log.e(Utils.LOG_DEBUG_TAG, sb.toString());
        query.findInBackground(callback);

    }

    @Override
    public void findExerciseById(String parseId, GetCallback<Exercise> callback) {
        ParseQuery<Exercise> query = ParseQuery.getQuery(Exercise.class);
        query.getInBackground(parseId, callback);
    }

    @Override
    public void findParticipants(Exercise exercise, FindCallback<ParseUser> callback) {

        ParseQuery<ParseUser> query = ParseQuery.getQuery(ParseUser.class);
        query.whereEqualTo(FIELD_USER_JOINED_EXERCISES, exercise);
        query.findInBackground(callback);
    }

    @Override
    public void findJoinedExercises(ParseUser currentUser, FindCallback<Exercise> findCallback) {
        ParseRelation relation = currentUser.getRelation(FIELD_USER_JOINED_EXERCISES);
        ParseQuery<Exercise> query = relation.getQuery();
        query.findInBackground(findCallback);
    }

    @Override
    public void findCreatedExercises(ParseUser currentUser, FindCallback<Exercise> findCallback) {
        ParseQuery<Exercise> query = ParseQuery.getQuery(Exercise.class);
        query.whereEqualTo(Exercise.FIELD_EX_CREATOR, currentUser);
        query.findInBackground(findCallback);
    }
}