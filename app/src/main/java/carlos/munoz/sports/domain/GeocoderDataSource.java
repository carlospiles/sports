package carlos.munoz.sports.domain;

import android.util.Log;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;

import bolts.Continuation;
import bolts.Task;
import carlos.munoz.sports.listener.AddressListener;
import carlos.munoz.sports.utils.Utils;

public class GeocoderDataSource {

    GeoApiContext context;
    int lastQuerySize;

    public GeocoderDataSource() {
        context = new GeoApiContext()
                .setApiKey("AIzaSyA3dkj3QzYj1nn377wYZ367XWMMr_-h9Ag")
                .setQueryRateLimit(10, 2);
    }

    public void findAddresses(String query, final AddressListener listener) {
        if(lastQuerySize > query.length()){
            // reduce queries by searching only when query size increases
            lastQuerySize = query.length();
        }else{
            findAddressesAsync(query).continueWith(new Continuation<GeocodingResult[], Void>() {
                @Override
                public Void then(Task<GeocodingResult[]> task) throws Exception {
                    if (task.isCancelled()) {
                        listener.onAddressError("Task cancelled");
                    } else if (task.isFaulted()) {
                        listener.onAddressError(task.getError().getMessage());
                    } else {
                        listener.onAddressSuccess(task.getResult());
                    }
                    return null;
                }
            });
        }
    }

    private Task<GeocodingResult[]> findAddressesAsync(String query) {

        Task<GeocodingResult[]>.TaskCompletionSource taskCompletion = Task.create();

        GeocodingResult[] addresses;
        try {
            lastQuerySize = query.length();
            addresses = GeocodingApi
                    .newRequest(context)
                    .address(query)
                    .await();
            if (addresses != null && addresses.length > 0) {
                taskCompletion.setResult(addresses);
            } else {
                taskCompletion.setResult(null);
            }
        } catch (Exception e) {
            Log.e(Utils.LOG_DEBUG_TAG, e.getMessage());
            taskCompletion.setError(e);
        }

        return taskCompletion.getTask();
    }
}
