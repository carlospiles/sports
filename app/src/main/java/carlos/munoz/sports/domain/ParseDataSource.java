package carlos.munoz.sports.domain;

import android.app.Activity;

import com.google.android.gms.maps.model.LatLng;
import com.parse.CountCallback;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.Date;

import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.listener.FacebookLoginListener;

public interface ParseDataSource {

    /**
     * ************* ACCOUNT CRUD ***************
     */

    public void signUp(String name, String email, String pass, SignUpCallback callback);

    public void logIn(String name, String pass, LogInCallback callback);

    public void facebookLogIn(Activity returnActivity, FacebookLoginListener callback);

    public void logOut();

    public void joinExercise(ParseUser participant,
                             ParseObject exercise, SaveCallback callback);

    public void leaveExercise(ParseUser participant,
                              ParseObject exercise, SaveCallback callback);

    public void findUserById(String parseUserId, GetCallback<ParseUser> callback);

    /**
     * @param user     User to check if is participant of the exercise
     * @param exercise to be ckecked
     * @param callback event with result count, could be 1 (joined) or 0 (unjoined)
     */
    public void checkIfUserJoined(ParseUser user, ParseObject exercise, CountCallback callback);


    /**
     * ************ EXERCISE CRUD **************
     */

    public void createExercise(int category, int level, String title,
                               String desc, ParseUser user,
                               double lat, double lon, Date startDate,
                               Date endDate, ArrayList<String> weekdays, SaveCallback callback);

    public void deleteExercise(Exercise exercise, DeleteCallback callback);

    public void findExercises(LatLng targetLocation, double radiusInKms,
                              Exercise filterExercise, FindCallback<Exercise> callback);

    public void findExerciseById(String parseId, GetCallback<Exercise> callback);

    public void findParticipants(Exercise exercise, FindCallback<ParseUser> callback);

    public void findJoinedExercises(ParseUser currentUser, FindCallback<Exercise> findCallback);

    public void findCreatedExercises(ParseUser currentUser, FindCallback<Exercise> findCallback);
}
