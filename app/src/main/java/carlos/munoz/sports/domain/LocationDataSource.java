package carlos.munoz.sports.domain;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

import carlos.munoz.sports.common.SportsApplication;
import carlos.munoz.sports.listener.LocationListener;
import carlos.munoz.sports.utils.Utils;

public class LocationDataSource {

    private static LocationDataSource instance;

    private static ArrayList<LocationListener> subscribers;

    private static GoogleApiClient mLocationClient;
    private static Location mLocation;

    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(3600000) // 60 minutes
            .setFastestInterval(60000) // 1 minute
            .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    private static LocationDataSource init() {
        if (instance == null) instance = getSync();
        return instance;
    }

    private static synchronized LocationDataSource getSync() {
        if (instance == null) instance = new LocationDataSource();
        return instance;
    }

    private LocationDataSource() {
        subscribers = new ArrayList<>();
    }

    public static void subscribe(LocationListener callback) {
        init();
        if (subscribers.isEmpty()) {
            Log.e(Utils.LOG_DEBUG_TAG, "FIRST SUBSCRIBER");
            setUpLocationClientIfNeeded();
            mLocationClient.connect();
        } else {
            Log.e(Utils.LOG_DEBUG_TAG,
                    "NOT FIRST SUBSCRIBER, subscribers: " + subscribers.size());
        }
        //send last known location
        if (mLocation != null) {
            callback.onUpdate(mLocation);
        }
        subscribers.add(callback);
    }

    public static void unSubscribe(LocationListener callback) {
        init();
        subscribers.remove(callback);
        if (subscribers.isEmpty()) {
            Log.e(Utils.LOG_DEBUG_TAG, "LAST SUBSCRIBER");
            mLocationClient.disconnect();
        } else {
            Log.e(Utils.LOG_DEBUG_TAG, "NOT LAST SUBSCRIBER, subscribers: " + subscribers.size());
        }
    }

    private static void setUpLocationClientIfNeeded() {

        if (mLocationClient == null) {

            mLocationClient = new GoogleApiClient.Builder(SportsApplication.get())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            LocationServices.FusedLocationApi.requestLocationUpdates(
                                    mLocationClient,
                                    REQUEST,
                                    new com.google.android.gms.location.LocationListener() {
                                        @Override
                                        public void onLocationChanged(Location location) {
                                            Log.e(Utils.LOG_DEBUG_TAG, "LOCATION CHANGED");
                                            mLocation = location;
                                            for (LocationListener subscriber : subscribers) {
                                                subscriber.onUpdate(location);
                                            }
                                        }
                                    });
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            Log.e(Utils.LOG_DEBUG_TAG, "Connection suspended");
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {
                            Log.e(Utils.LOG_DEBUG_TAG, "Connection failed");
                        }
                    })
                    .build();
        }
    }
}
