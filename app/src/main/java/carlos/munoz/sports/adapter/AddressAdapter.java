package carlos.munoz.sports.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.google.maps.model.GeocodingResult;

public class AddressAdapter extends CursorAdapter {

    private GeocodingResult[] addresses;

    private TextView text;

    public AddressAdapter(Context context, Cursor cursor, GeocodingResult[] addresses) {

        super(context, cursor, false);
        this.addresses = addresses;

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        text.setText(addresses[cursor.getPosition()].formattedAddress);
    }

    public GeocodingResult getAddress(int position) {
        return addresses[position];
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        text = (TextView) view.findViewById(android.R.id.text1);

        return view;

    }
}