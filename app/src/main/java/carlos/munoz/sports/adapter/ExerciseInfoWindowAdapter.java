package carlos.munoz.sports.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.Map;

import carlos.munoz.sports.R;
import carlos.munoz.sports.entities.Exercise;

/**
 * Created by carlos on 24/1/15.
 */
public class ExerciseInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    Activity context;
    Map<Marker, Exercise> allMarkersMap;

    public ExerciseInfoWindowAdapter(Map<Marker, Exercise> allMarkersMap, Activity context) {
        this.allMarkersMap = allMarkersMap;
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        Exercise exercise = allMarkersMap.get(marker);
        View v = context.getLayoutInflater().inflate(R.layout.view_infowindow_exercise, null);

        TextView tvTitle = (TextView) v.findViewById(R.id.tv_title);
        TextView tvDesc = (TextView) v.findViewById(R.id.tv_desc);

        tvTitle.setText(exercise.getTitle());
        tvDesc.setText(exercise.getDescription());

        return v;
    }
}
