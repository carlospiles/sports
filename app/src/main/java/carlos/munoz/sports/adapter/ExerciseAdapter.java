package carlos.munoz.sports.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import carlos.munoz.sports.R;
import carlos.munoz.sports.entities.Exercise;

public class ExerciseAdapter extends ArrayAdapter<Exercise> {

    private Context context;
    private List<Exercise> data;
    private TypedArray sportMapIcons;


    public ExerciseAdapter(Context context, List<Exercise> data) {
        super(context, R.layout.row_exercise, R.id.firstLine, data);
        this.context = context;
        this.data = data;
        this.sportMapIcons = context.getResources().obtainTypedArray(R.array.sport_icons);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public Exercise getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ExerciseHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.row_exercise, parent, false);

            holder = new ExerciseHolder(row);

            row.setTag(holder);
        } else {
            holder = (ExerciseHolder) row.getTag();
        }

        Exercise exercise = data.get(position);
        holder.exerciseTitle.setText(exercise.getTitle());
        holder.exerciseDesc.setText(exercise.getDescription());
        Picasso.with(context)
                .load(sportMapIcons.getResourceId(exercise.getSport(), 0))
                .into(holder.exerciseImage);
        return row;
    }

    class ExerciseHolder {

        TextView exerciseTitle, exerciseDesc;
        ImageView exerciseImage;

        ExerciseHolder(View row) {
            exerciseTitle = (TextView) row.findViewById(R.id.firstLine);
            exerciseDesc = (TextView) row.findViewById(R.id.secondLine);
            exerciseImage = (ImageView) row.findViewById(R.id.icon);
        }
    }
}