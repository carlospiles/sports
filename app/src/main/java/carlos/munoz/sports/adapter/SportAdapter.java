package carlos.munoz.sports.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import carlos.munoz.sports.R;
import carlos.munoz.sports.entities.Sport;

public class SportAdapter extends ArrayAdapter<Sport> {

    Context context;
    ArrayList<Sport> data = null;

    public SportAdapter(Context context,
                        ArrayList<Sport> data) {
        super(context, R.layout.row_sport, R.id.sport_title_textView, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public Sport getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SportHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.row_sport, parent, false);

            holder = new SportHolder(row);

            row.setTag(holder);
        } else {
            holder = (SportHolder) row.getTag();
        }

        Sport sport = data.get(position);
        holder.sportTitle.setText(sport.getName());
        Picasso.with(context).load(sport.getImgResource()).into(holder.sportImage);
        return row;
    }

    class SportHolder {

        TextView sportTitle;
        ImageView sportImage;

        SportHolder(View row) {
            sportTitle = (TextView) row.findViewById(R.id.sport_title_textView);
            sportImage = (ImageView) row.findViewById(R.id.sport_icon_imageView);
        }
    }
}