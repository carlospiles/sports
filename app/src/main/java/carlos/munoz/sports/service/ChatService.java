package carlos.munoz.sports.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.orm.query.Select;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.net.URISyntaxException;
import java.util.List;

import carlos.munoz.sports.R;
import carlos.munoz.sports.domain.ParseDataSource;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.entities.Message;
import carlos.munoz.sports.ui.home.HomeActivity;
import carlos.munoz.sports.utils.Utils;

/**
 * Created by carlos on 27/1/15.
 */
public class ChatService extends IntentService {

    public static final String BROADCAST_NEW_MESSAGE =
            "carlos.munoz.sports.ACTION";

    public static final String BROADCAST_EXTRA_ROOM =
            "carlos.munoz.sports.STATUS";

    public static final int ACTION_SUSCRIBE = 3;
    public static final int ACTION_UNSUSCRIBE = 2;
    public static final int ACTION_SEND = 1;
    public static final String KEY_ACTION = "chat_action";

    ParseDataSource parseDataSource;
    ParseUser user;

    private Socket mSocket;

    {
        try {
            IO.Options opts = new IO.Options();
            opts.reconnection = true;
            mSocket = IO.socket("https://polar-everglades-6142.herokuapp.com/");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public ChatService() {
        super("ChatService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        parseDataSource = new ParseDataSourceImpl();
        user = ParseUser.getCurrentUser();

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on("newmessage", onNewMessage);
        mSocket.connect();
        Log.e(Utils.LOG_DEBUG_TAG, "SERVICE CREATED");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String room = intent.getStringExtra("room");
            switch (intent.getIntExtra(KEY_ACTION, 0)) {
                case ACTION_SEND:
                    String message = intent.getStringExtra("message");
                    if (room != null && message != null) {
                        attemptSend(message, room, user.getUsername(), user.getObjectId());
                    }
                    break;
                case ACTION_SUSCRIBE:
                    Log.e(Utils.LOG_DEBUG_TAG, "SUSCRIBED TO SOCKET BY BUTTON");

                    mSocket.emit("adduser", user.getUsername(), user.getObjectId(), room, 0);
                    break;
                case ACTION_UNSUSCRIBE:
                    Log.e(Utils.LOG_DEBUG_TAG, "UNSUSCRIBED TO SOCKET BY BUTTON");
                    mSocket.emit("removeuser", user.getObjectId(), room);
                    break;
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        Log.e(Utils.LOG_DEBUG_TAG, "SERVICE DESTROYED");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {
        Log.e(Utils.LOG_DEBUG_TAG, "RECEIVED INTENT ON SERVICE");
        String room = workIntent.getStringExtra("room");
        String message = workIntent.getStringExtra("message");
        attemptSend(message, room, user.getUsername(), user.getObjectId());
    }

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e(Utils.LOG_DEBUG_TAG, "ERROR CONNECTING TO SOCKET" + args[0]);
        }
    };

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e(Utils.LOG_DEBUG_TAG, "Connected to socket");
            if (user != null) {
                parseDataSource.findJoinedExercises(user, new FindCallback<Exercise>() {
                    @Override
                    public void done(List<Exercise> exercises, ParseException e) {
                        Message last = Select.from(Message.class).orderBy("message_id").first();
                        String msgId;
                        try {
                            msgId = last.getMessageId();
                        } catch (NullPointerException e2) {
                            msgId = "0";
                        }
                        Log.e(Utils.LOG_DEBUG_TAG, "LAST MESSAGE RECEIVED: " + msgId);
                        for (Exercise ex : exercises) {
                            mSocket.emit("adduser", user.getUsername(), user.getObjectId(), ex.getObjectId(), msgId);
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            String userName = (String) args[0];
            String userId = (String) args[1];
            String messageContent = (String) args[2];
            String roomId = (String) args[3];
            String messageId = "" + args[4];

            Log.e(Utils.LOG_DEBUG_TAG, "NEW MESSAGE " + userName + " " + messageContent + " " + messageId);

            //TODO arreglar
            Message message = new Message(roomId, userName, messageContent, userId, messageId);
            if (!userId.equals("SERVER")) {
                //1. store message on db (if not server message)
                message.save();
                //2. show notification
                sendNotification(message.getUserId(), message.getMessageContent());
            }

            //3. alert existing activities that new message has arrived.
            broadcastMessage(message.getRoomId());

        }
    };


    private void attemptSend(String content, String room, String userName, String userId) {
        if (!mSocket.connected()) return;

        Log.e(Utils.LOG_DEBUG_TAG, "SEND MESSAGE " + content + " " + room + " " + userName + " " + userId);
        // perform the sending message attempt.
        mSocket.emit("sendchat", content, room, userName, userId);
    }

    /**
     * Send broadcast to alert existing activity that new messages have been received.
     *
     * @param roomId room with new messages.
     */
    private void broadcastMessage(String roomId) {
        Intent localIntent = new Intent(BROADCAST_NEW_MESSAGE);
        localIntent.putExtra(BROADCAST_EXTRA_ROOM, roomId);
        Log.e(Utils.LOG_DEBUG_TAG, "SENDING BROADCAST");
        sendBroadcast(localIntent);
    }

    /**
     * Shows notification with new message.
     *
     * @param title
     * @param text
     */
    private void sendNotification(String title, String text) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(text);

        Intent resultIntent = new Intent(this, HomeActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(123456, mBuilder.build());
    }


}
