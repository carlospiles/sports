package carlos.munoz.sports.listener;

public interface FacebookLoginListener {
    public void onSuccess();

    public void onError(String errorMsg);
}
