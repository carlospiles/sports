package carlos.munoz.sports.listener;

import java.util.ArrayList;

public interface WeekdaysChangedListener {
    public void onWeekdaysChanged(ArrayList<String> weekdays);
}
