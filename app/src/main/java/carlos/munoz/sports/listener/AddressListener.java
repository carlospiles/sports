package carlos.munoz.sports.listener;


import com.google.maps.model.GeocodingResult;

public interface AddressListener {
    public void onAddressSuccess(GeocodingResult[] addresses);

    public void onAddressError(String errorMsg);
}
