package carlos.munoz.sports.listener;

import android.location.Location;

public interface LocationListener {
    public void onUpdate(Location location);
}
