package carlos.munoz.sports.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import java.util.ArrayList;

import carlos.munoz.sports.R;
import carlos.munoz.sports.listener.WeekdaysChangedListener;

public class WeekdaysDialog extends DialogFragment {


    boolean checks[];

    public WeekdaysDialog() {
    }

    public static WeekdaysDialog newInstance(String[] weekdays) {
        WeekdaysDialog f = new WeekdaysDialog();
        Bundle args = new Bundle();
        args.putStringArray("weekdays", weekdays);
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final String[] weekdays = getArguments().getStringArray("weekdays");
        final boolean[] checks = new boolean[weekdays.length];

        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setTitle(R.string.newexercise_dialog_title_weekdays)
                .setMultiChoiceItems(weekdays, checks,
                        new DialogInterface.OnMultiChoiceClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which, boolean isChecked) {
                                checks[which] = isChecked;
                            }
                        })
                .setPositiveButton(R.string.accept,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                ArrayList<String> selectedWeekdays = new ArrayList<String>();
                                for (int i = 0; i < checks.length; i++) {
                                    if (checks[i]) {
                                        selectedWeekdays.add(weekdays[i]);
                                    }
                                }
                                try {
                                    ((WeekdaysChangedListener) getActivity())
                                            .onWeekdaysChanged(selectedWeekdays);
                                } catch (ClassCastException e) {
                                    //activity doesn't implement callback,
                                    // try to get invocator fragment instead
                                    ((WeekdaysChangedListener) getTargetFragment())
                                            .onWeekdaysChanged(selectedWeekdays);
                                } catch (Exception e) {
                                    //catch every other exception
                                    e.printStackTrace();
                                }
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                            }
                        });
        return builder.create();
    }
}