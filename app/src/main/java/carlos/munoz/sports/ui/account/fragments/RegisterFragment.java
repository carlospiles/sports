package carlos.munoz.sports.ui.account.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import carlos.munoz.sports.R;
import carlos.munoz.sports.ui.account.presenter.LoginPresenter;
import carlos.munoz.sports.ui.account.presenter.LoginPresenterImpl;
import carlos.munoz.sports.ui.account.presenter.LoginUi;

public class RegisterFragment extends Fragment {

    EditText mUserName, mEmail, mPass;
    Button mActionBtn;

    View rootView;
    LoginPresenter presenter;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    public RegisterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (rootView == null) {

            rootView = inflater.inflate(R.layout.fragment_register, container, false);
            presenter = new LoginPresenterImpl((LoginUi) getActivity());

            mUserName = (EditText) rootView.findViewById(R.id.accountUsername);
            mEmail = (EditText) rootView.findViewById(R.id.accountEmail);
            mPass = (EditText) rootView.findViewById(R.id.accountPassword);
            mActionBtn = (Button) rootView.findViewById(R.id.accountAction);

            mActionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.doSignUp(
                            mUserName.getText().toString(),
                            mEmail.getText().toString(),
                            mPass.getText().toString());
                }
            });
        }


        return rootView;
    }

}
