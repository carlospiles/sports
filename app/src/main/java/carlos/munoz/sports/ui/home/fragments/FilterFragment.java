package carlos.munoz.sports.ui.home.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carlos.munoz.sports.R;
import carlos.munoz.sports.adapter.SportAdapter;
import carlos.munoz.sports.dialogs.WeekdaysDialog;
import carlos.munoz.sports.domain.PreferenceDataSource;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.listener.WeekdaysChangedListener;
import carlos.munoz.sports.utils.Utils;

public class FilterFragment extends Fragment implements TimePickerDialog.OnTimeSetListener, WeekdaysChangedListener {

    public static final String TIMEPICKER_TAG_START = "timepicker_start";
    public static final String TIMEPICKER_TAG_END = "timepicker_end";

    final Calendar calendar = Calendar.getInstance();
    final TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this,
            calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true, true);

    View rootView;
    Spinner mSportSpinner;
    RadioGroup mLevelRg;
    Button mStartDateBtn, mEndDateBtn, mWeekdaysBtn;
    CheckBox mAllDateChk, mAllSportChk, mAllLevelChk;

    Exercise filterExercise;
    boolean filterExerciseChanged = false;
    PreferenceDataSource preferenceDS;

    public FilterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_filter, container, false);

            mSportSpinner = (Spinner) rootView.findViewById(R.id.sport_spinner);
            mLevelRg = (RadioGroup) rootView.findViewById(R.id.level_radiogroup);
            mStartDateBtn = (Button) rootView.findViewById(R.id.button_start_date);
            mEndDateBtn = (Button) rootView.findViewById(R.id.button_end_date);
            mWeekdaysBtn = (Button) rootView.findViewById(R.id.button_weekdays);
            mAllDateChk = (CheckBox) rootView.findViewById(R.id.checkbox_all_dates);
            mAllSportChk = (CheckBox) rootView.findViewById(R.id.checkbox_all_sports);
            mAllLevelChk = (CheckBox) rootView.findViewById(R.id.checkbox_all_levels);

            SportAdapter adapter = new SportAdapter(getActivity(),
                    Utils.generateSportData(getActivity()));
            mSportSpinner.setAdapter(adapter);

            preferenceDS = new PreferenceDataSource(getActivity());
            filterExercise = preferenceDS.readFilterExercise();

            refreshUi();

            mAllDateChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        filterExercise.setStartDate(new Date(100000000));
                        filterExercise.setEndDate(new Date(100000000));
                        filterExercise.setWeekDays(new ArrayList<String>());
                    } else {
                        filterExercise.setStartDate(new Date(0, 0, 0, 15, 0));
                        filterExercise.setStartDate(new Date(0, 0, 0, 20, 0));
                        ArrayList<String> weekdays = new ArrayList<>();
                        filterExercise.setWeekDays(weekdays);
                    }
                    filterExerciseChanged = true;
                    refreshUi();
                }
            });

            mAllSportChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        filterExercise.setSport(-1);
                    } else {
                        filterExercise.setSport(0);
                    }
                    filterExerciseChanged = true;
                    refreshUi();
                }
            });

            mAllLevelChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        filterExercise.setLevel(-1);
                    } else {
                        filterExercise.setLevel(1);
                    }
                    filterExerciseChanged = true;
                    refreshUi();
                }
            });

            mSportSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    filterExercise.setSport(position);
                    filterExerciseChanged = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mLevelRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.radioButton1:
                            filterExercise.setLevel(1);
                            break;
                        case R.id.radioButton2:
                            filterExercise.setLevel(2);
                            break;
                        case R.id.radioButton3:
                            filterExercise.setLevel(3);
                            break;
                    }
                    filterExerciseChanged = true;
                }
            });

            mStartDateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timePickerDialog.setCloseOnSingleTapMinute(false);
                    timePickerDialog.show(((FragmentActivity) getActivity())
                            .getSupportFragmentManager(), TIMEPICKER_TAG_START);
                }
            });

            mEndDateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timePickerDialog.setCloseOnSingleTapMinute(false);
                    timePickerDialog.show(((FragmentActivity) getActivity())
                            .getSupportFragmentManager(), TIMEPICKER_TAG_END);
                }
            });

            mWeekdaysBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WeekdaysDialog df = WeekdaysDialog.newInstance(getResources()
                            .getStringArray(R.array.weekdays));
                    df.setTargetFragment(FilterFragment.this, 10);
                    df.show(getFragmentManager(), "DIALOG_WEEKDAYS");
                }
            });
        }
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(Utils.LOG_DEBUG_TAG, "onpause filter");
        preferenceDS.writeFilterExercise(filterExercise);
        preferenceDS.writeFilterChanged(filterExerciseChanged);
        filterExerciseChanged = false;
    }

    private void refreshUi() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        //sport card refresh
        if (filterExercise.getSport() < 0) {
            mAllSportChk.setChecked(true);
            rootView.findViewById(R.id.card_sports).setVisibility(View.GONE);
        } else {
            mAllSportChk.setChecked(false);
            mSportSpinner.setSelection(filterExercise.getSport());
            rootView.findViewById(R.id.card_sports).setVisibility(View.VISIBLE);
        }

        //level card refresh
        if (filterExercise.getLevel() < 0) {
            mAllLevelChk.setChecked(true);
            rootView.findViewById(R.id.card_levels).setVisibility(View.GONE);

        } else {
            mAllLevelChk.setChecked(false);
            ((RadioButton) mLevelRg.getChildAt(filterExercise.getLevel() - 1))
                    .setChecked(true);
            rootView.findViewById(R.id.card_levels).setVisibility(View.VISIBLE);
        }
        //date card refresh
        if (filterExercise.getStartDate().getTime() == 100000000
                && filterExercise.getEndDate().getTime() == 100000000
                && filterExercise.getWeekDays().isEmpty()) {
            mAllDateChk.setChecked(true);
            rootView.findViewById(R.id.card_dates).setVisibility(View.GONE);
        } else {
            mAllDateChk.setChecked(false);
            rootView.findViewById(R.id.card_dates).setVisibility(View.VISIBLE);
            if (filterExercise.getStartDate().getTime() != 100000000) {
                mStartDateBtn.setText("Start time: "
                        + sdf.format(filterExercise.getStartDate()));
            } else {
                mStartDateBtn.setText("Start time (not selected)");
            }
            if (filterExercise.getEndDate().getTime() != 100000000) {
                mEndDateBtn.setText("End time: "
                        + sdf.format(filterExercise.getEndDate()));
            } else {
                mEndDateBtn.setText("End time (not selected)");
            }
            if (filterExercise.getWeekDays().isEmpty()) {
                mWeekdaysBtn.setText("Week days (not selected)");
            } else {
                StringBuilder sb = new StringBuilder("Week days: ");
                for (int i = 0; i < filterExercise.getWeekDays().size(); i++) {
                    sb.append(filterExercise.getWeekDays().get(i));
                    if (i == filterExercise.getWeekDays().size() - 2) {
                        sb.append("s and ");
                    } else {
                        sb.append("s, ");
                    }
                }
                mWeekdaysBtn.setText(sb.toString());
            }
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minute) {
        String tag = timePickerDialog.getTag();
        if (tag.equalsIgnoreCase(TIMEPICKER_TAG_START)) {
            filterExercise.setStartDate(new Date(0, 0, 0, hourOfDay, minute));
        } else if (tag.equalsIgnoreCase(TIMEPICKER_TAG_END)) {
            filterExercise.setEndDate(new Date(0, 0, 0, hourOfDay, minute));
        }
        filterExerciseChanged = true;
        refreshUi();
    }

    @Override
    public void onWeekdaysChanged(ArrayList<String> weekdays) {
        filterExercise.setWeekDays(weekdays);
        filterExerciseChanged = true;
        refreshUi();
        Log.e(Utils.LOG_DEBUG_TAG, "" + weekdays.size());
    }
}
