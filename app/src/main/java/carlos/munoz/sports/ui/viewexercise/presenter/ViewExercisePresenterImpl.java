package carlos.munoz.sports.ui.viewexercise.presenter;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;

import carlos.munoz.sports.domain.ParseDataSource;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.service.ChatService;
import carlos.munoz.sports.ui.viewexercise.ViewExerciseUi;
import carlos.munoz.sports.utils.Utils;

public class ViewExercisePresenterImpl implements ViewExercisePresenter {

    ViewExerciseUi ui;
    ParseDataSource parseDataSource;
    Exercise currentExercise;
    ParseUser currentUser;

    public ViewExercisePresenterImpl(ViewExerciseUi ui) {
        this.ui = ui;
        parseDataSource = new ParseDataSourceImpl();
        currentUser = ParseUser.getCurrentUser();
    }

    @Override
    public void findExercise(String parseId) {
        ui.showProgressDialog("Loading activity", "please wait");
        parseDataSource.findExerciseById(parseId, new GetCallback<Exercise>() {
            @Override
            public void done(Exercise exercise, ParseException e) {

                currentExercise = exercise;

                if (e == null) {
                    ui.fillUi(exercise);
                    if (currentUser == null) {
                        ui.switchToUnLoggedUi();
                        ui.hideProgressDialog();
                    } else {
                        checkIfUserJoined();
                    }
                } else {
                    ui.showToast(e.getMessage());
                    ui.hideProgressDialog();
                }
            }
        });
    }

    private void checkIfUserJoined() {
        parseDataSource.checkIfUserJoined(currentUser, currentExercise, new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                Log.e(Utils.LOG_DEBUG_TAG, "joined: " + i);
                if (i > 0) {
                    ui.switchToJoinedUi();
                } else {
                    ui.switchToUnJoinedUi();
                }
                ui.hideProgressDialog();
                findParticipants();
            }
        });
    }

    private void findParticipants() {
        parseDataSource.findParticipants(currentExercise, new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> parseUsers, ParseException e) {
                ui.fillUserListUi(parseUsers);
            }
        });
    }

    @Override
    public void joinExercise(final Context context) {
        ui.showProgressDialog("Joining activity", "please wait");
        if (currentUser != null) {
            parseDataSource.joinExercise(currentUser, currentExercise, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Intent i = new Intent(context, ChatService.class);
                        i.putExtra(ChatService.KEY_ACTION, ChatService.ACTION_SUSCRIBE);
                        i.putExtra("room", currentExercise.getObjectId());
                        context.startService(i);
                        ui.showToast("Succesfully joined!");
                        ui.switchToJoinedUi();
                    } else {
                        ui.showToast(e.getMessage());
                    }
                }
            });
        } else {
            ui.showToast("You must be logged to join activities!");
        }
        ui.hideProgressDialog();
    }

    @Override
    public void leaveExercise(final Context context) {
        ui.showProgressDialog("Leaving activity", "please wait");
        if (currentUser != null) {
            parseDataSource.leaveExercise(currentUser, currentExercise, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Intent i = new Intent(context, ChatService.class);
                        i.putExtra(ChatService.KEY_ACTION, ChatService.ACTION_UNSUSCRIBE);
                        i.putExtra("room", currentExercise.getObjectId());
                        context.startService(i);
                        ui.showToast("Succesfully leaved!");
                        ui.switchToUnJoinedUi();
                    } else {
                        ui.showToast(e.getMessage());
                    }
                }
            });
        } else {
            ui.showToast("You must be logged to leave activities!");
        }
        ui.hideProgressDialog();
    }
}
