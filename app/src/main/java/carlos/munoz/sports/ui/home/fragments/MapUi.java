package carlos.munoz.sports.ui.home.fragments;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public interface MapUi {

    /**
     * Moves the map camera
     *
     * @param location    center of the movement
     * @param radiusInKms used to calculate zoom level
     */
    public void moveMapCamera(LatLng location, int radiusInKms);

    /**
     * Draws a blue translucent circle on the map
     *
     * @param location    center of the circle
     * @param radiusInKms radius of the circle
     */
    public void drawMapCircle(LatLng location, int radiusInKms);

    /**
     * @param location place to put the marker
     * @param title    shown when marker clicked
     * @param snippet  description shown when marker clicked
     * @param sportId  used to find image resource
     * @return drawn marker
     */
    public Marker drawMarker(LatLng location, String title, String snippet, int sportId);
}
