package carlos.munoz.sports.ui.home.presenter;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public interface MapPresenter {

    public void findExercises(LatLng targetLocation, Activity context, GoogleMap map);

    public void openExercise(Marker marker, Context context);

}
