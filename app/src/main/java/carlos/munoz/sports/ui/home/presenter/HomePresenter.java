package carlos.munoz.sports.ui.home.presenter;

import android.content.Context;

public interface HomePresenter {

    public void initDrawer(Context context);

    public void findAddresses(String newText);
}
