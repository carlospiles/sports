package carlos.munoz.sports.ui.account.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import carlos.munoz.sports.R;
import carlos.munoz.sports.ui.account.presenter.LoginPresenter;
import carlos.munoz.sports.ui.account.presenter.LoginPresenterImpl;
import carlos.munoz.sports.ui.account.presenter.LoginUi;

public class LoginFragment extends Fragment {

    EditText mUserName, mPass;
    View mActionBtn, mLoginFacebookBtn;

    View rootView;

    LoginPresenter presenter;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (rootView == null) {

            rootView = inflater.inflate(R.layout.fragment_login, container, false);
            presenter = new LoginPresenterImpl((LoginUi) getActivity());

            mUserName = (EditText) rootView.findViewById(R.id.accountUsername);
            mPass = (EditText) rootView.findViewById(R.id.accountPassword);
            mActionBtn = rootView.findViewById(R.id.accountAction);
            mLoginFacebookBtn = rootView.findViewById(R.id.accountLoginFacebook);

            mActionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.doLogin(
                            mUserName.getText().toString(),
                            mPass.getText().toString(),
                            getActivity());
                }
            });

            mLoginFacebookBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.doFacebookLogin(getActivity());
                }
            });
        }

        return rootView;
    }

}
