package carlos.munoz.sports.ui.viewexercise.presenter;

import android.content.Context;

import carlos.munoz.sports.entities.Exercise;

public interface ViewExercisePresenter {

    public void findExercise(String parseId);

    public void joinExercise(Context context);

    public void leaveExercise(Context context);
}
