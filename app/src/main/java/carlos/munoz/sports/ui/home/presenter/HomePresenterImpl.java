package carlos.munoz.sports.ui.home.presenter;

import android.content.Context;

import com.google.maps.model.GeocodingResult;
import com.parse.ParseUser;

import carlos.munoz.sports.R;
import carlos.munoz.sports.domain.GeocoderDataSource;
import carlos.munoz.sports.listener.AddressListener;
import carlos.munoz.sports.ui.home.HomeUi;
import carlos.munoz.sports.utils.Utils;

public class HomePresenterImpl implements HomePresenter {

    HomeUi ui;
    GeocoderDataSource mGeocoderDataSource;

    public HomePresenterImpl(HomeUi ui) {
        this.ui = ui;
        this.mGeocoderDataSource = new GeocoderDataSource();
    }

    public void initDrawer(Context context) {
        ParseUser currentUser = ParseUser.getCurrentUser();

        if (currentUser == null) {

            //not logged, load "Log in" data
            ui.fillDrawer(
                    "Log in or create account",
                    "",
                    context.getResources().getStringArray(R.array.drawer_titles));
        } else {
            if (currentUser.getString("fbId") == null) {

                //not logged with facebook, load parseUser data

                ui.showToast("Logged with Parse");
                ui.fillDrawer(
                        currentUser.getUsername(),
                        "",
                        context.getResources().getStringArray(R.array.drawer_titles_logged));
            } else {

                //logged with facebook, load facebook data

                ui.showToast("Logged with Facebook");
                ui.fillDrawer(
                        currentUser.getUsername(),
                        Utils.buildFbPictureUrl(currentUser.getString("fbId")),
                        context.getResources().getStringArray(R.array.drawer_titles_logged));
            }
        }
    }

    @Override
    public void findAddresses(String newText) {
        // search starting when query has at least 4 chars
        if (newText.length() > 4) {
            mGeocoderDataSource.findAddresses(newText, new AddressListener() {
                @Override
                public void onAddressSuccess(GeocodingResult[] addresses) {
                    ui.setSearchSuggestionsAdapter(addresses);
                }

                @Override
                public void onAddressError(String errorMsg) {
                    ui.setSearchSuggestionsAdapter(null);
                }
            });
        } else {
            ui.setSearchSuggestionsAdapter(null);
        }
    }
}
