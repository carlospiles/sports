package carlos.munoz.sports.ui.account.presenter;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.List;

import carlos.munoz.sports.common.BaseUi;
import carlos.munoz.sports.domain.ParseDataSource;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.domain.PreferenceDataSource;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.ui.account.fragments.MyActivitiesUi;


public class MyActivitiesPresenterImpl implements MyActivitiesPresenter {

    ParseDataSource parseDS;
    PreferenceDataSource prefDS;
    BaseUi activityUi;
    MyActivitiesUi ui;

    public MyActivitiesPresenterImpl(BaseUi activityUi, MyActivitiesUi ui) {
        parseDS = new ParseDataSourceImpl();
        //prefDS = new PreferenceDataSource();
        this.activityUi = activityUi;
        this.ui = ui;
    }

    @Override
    public void getCreatedExercises() {
        parseDS.findCreatedExercises(ParseUser.getCurrentUser(), new FindCallback<Exercise>() {
            @Override
            public void done(List<Exercise> exercises, ParseException e) {
                if (e == null) {
                    activityUi.showToast("Created exercises: " + exercises.size());
                    ui.fillList(exercises, "no created exercises found!");
                } else {
                    activityUi.showToast(e.getMessage());
                }
            }
        });
    }

    @Override
    public void getParticipantExercises() {
        parseDS.findJoinedExercises(ParseUser.getCurrentUser(), new FindCallback<Exercise>() {
            @Override
            public void done(List<Exercise> exercises, ParseException e) {
                if (e == null) {
                    activityUi.showToast("Joined exercises: " + exercises.size());
                    ui.fillList(exercises, "no joined exercises found!");
                } else {
                    activityUi.showToast(e.getMessage());
                }
            }
        });
    }

    @Override
    public void getNearYouExercises() {

    }

    @Override
    public void openExercise() {

    }

    @Override
    public void deleteExercise() {

    }
}
