package carlos.munoz.sports.ui.newexercise.presenter;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Date;

import carlos.munoz.sports.R;
import carlos.munoz.sports.common.BaseUi;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.domain.ParseDataSource;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.entities.Sport;

public class NewExercisePresenterImpl implements NewExercisePresenter {

    BaseUi ui;
    ParseDataSource datasource;
    Context context;

    public NewExercisePresenterImpl(BaseUi ui, Context context) {
        this.datasource = new ParseDataSourceImpl();
        this.ui = ui;
        this.context = context;
    }

    @Override
    public void createNewExercise(String title, String desc, Sport selectedSport,
                                  int selectedLevelResId, LatLng location,
                                  Date startDate, Date endDate, ArrayList<String> weekdays) {

        ui.showProgressDialog("Creating new activity", "Wait, please");

        if (title == null || title.isEmpty() || desc == null
                || desc.isEmpty() || selectedSport == null
                || startDate == null || endDate == null
                || location == null || weekdays == null) {

            ui.hideProgressDialog();
            ui.showToast("All fields are required");

        } else if (ParseUser.getCurrentUser() == null) {

            ui.hideProgressDialog();
            ui.showToast("You must be registered to create activity");

        } else {

            int level = 0;
            switch (selectedLevelResId) {
                case R.id.radioButton1:
                    level = 1;
                    break;
                case R.id.radioButton2:
                    level = 2;
                    break;
                case R.id.radioButton3:
                    level = 3;
                    break;
            }

            datasource.createExercise(selectedSport.getCode(), level, title,
                    desc, ParseUser.getCurrentUser(), location.latitude,
                    location.longitude, startDate, endDate,
                    weekdays, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            ui.hideProgressDialog();
                            ui.showToast("Your activity was created successfully!");
                            Navigator.getInstance().goToMain(context);
                        }
                    });
        }
    }
}
