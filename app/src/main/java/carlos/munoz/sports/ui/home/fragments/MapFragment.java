package carlos.munoz.sports.ui.home.fragments;

import android.app.Fragment;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonFloat;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseUser;

import carlos.munoz.sports.R;
import carlos.munoz.sports.common.BaseUi;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.domain.LocationDataSource;
import carlos.munoz.sports.domain.PreferenceDataSource;
import carlos.munoz.sports.listener.LocationListener;
import carlos.munoz.sports.ui.home.HomeActivity;
import carlos.munoz.sports.ui.home.presenter.MapPresenter;
import carlos.munoz.sports.ui.home.presenter.MapPresenterImpl;
import carlos.munoz.sports.utils.Utils;

public class MapFragment extends Fragment
        implements MapUi, LocationListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMyLocationButtonClickListener {

    MapView mapView;
    public GoogleMap map;
    View rootView;
    SeekBar mRadiusSlider;
    TextView mRadiusTextView;

    ButtonFloat mNewExerciseBtn;

    public MapPresenter presenter;
    public PreferenceDataSource ds;

    LatLng lastTarget;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MapPresenterImpl(this, (HomeActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);

        if (rootView == null) {

            Log.e(Utils.LOG_DEBUG_TAG, "first load, read filters");

            rootView = inflater.inflate(R.layout.fragment_map, container, false);
            ds = new PreferenceDataSource(getActivity());

            //Button init
            mNewExerciseBtn = (ButtonFloat) rootView.findViewById(R.id.add_exercise_button);
            if (ParseUser.getCurrentUser() == null){
                mNewExerciseBtn.setVisibility(View.GONE);
            }else{
                mNewExerciseBtn.setVisibility(View.VISIBLE);
            }

            //Map init
            mapView = (MapView) rootView.findViewById(R.id.mapview);
            mapView.onCreate(savedInstanceState);
            map = mapView.getMap();
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            map.setOnInfoWindowClickListener(this);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setMyLocationEnabled(true);
            map.setOnMyLocationButtonClickListener(this);

            MapsInitializer.initialize(this.getActivity());

            //Slider init
            mRadiusTextView = (TextView) rootView.findViewById(R.id.seekbarText);
            mRadiusTextView.setText(ds.getRadius() + " kms");
            mRadiusSlider = (SeekBar) rootView.findViewById(R.id.seekBar);
            mRadiusSlider.setProgress(ds.getRadius());
            mRadiusSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (progress < 1) {
                        progress = 1;
                    }
                    mRadiusTextView.setText(progress + " kms");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    ds.setRadius(Math.max(1,mRadiusSlider.getProgress()));
                    presenter.findExercises(lastTarget, getActivity(), map);
                }
            });

            //Button init
            mNewExerciseBtn.bringToFront();
            mNewExerciseBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mNewExerciseBtn.setDrawableIcon(getResources().getDrawable(R.drawable.ic_add_white));
            mNewExerciseBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Navigator.getInstance().goToNewExercise(getActivity());
                }
            });

            ((BaseUi) getActivity())
                    .showTutorial("map", "map", BaseUi.TUTORIAL_MAP, mNewExerciseBtn);

        } else {
            PreferenceDataSource preferenceDataSource = new PreferenceDataSource(getActivity());
            if (preferenceDataSource.readFilterChanged()) {
                Log.e(Utils.LOG_DEBUG_TAG, "filters changed! reload");
                if (lastTarget == null) {
                    //if past location doesn't exist, try to get current location
                    LocationDataSource.subscribe(this);
                } else {
                    presenter.findExercises(lastTarget, getActivity(), map);
                }
            } else {
                Log.e(Utils.LOG_DEBUG_TAG, "filters didn't change");
            }
            preferenceDataSource.writeFilterChanged(false);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void moveMapCamera(LatLng location, int radiusInKms) {
        lastTarget = location;

        if (map != null) {
            map.clear();
            LatLngBounds bounds = Utils.boundsWithCenterAndLatLngDistance(
                    new LatLng(location.latitude,
                            location.longitude),
                    radiusInKms * 2000, radiusInKms * 2000);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 0);
            map.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void drawMapCircle(LatLng location, int radiusInKms) {
        if (map != null) {
            map.clear();
            map.addCircle(new CircleOptions().center(location)
                    .fillColor(Color.argb((int) (0.2 * 255), 0, 255, 255))
                    .strokeColor(Color.TRANSPARENT)
                    .radius(radiusInKms * 1000));
        }

    }

    @Override
    public Marker drawMarker(LatLng location, String title, String snippet, int sportId) {
        if (map != null) {
            TypedArray sportMapIcons = getResources().obtainTypedArray(R.array.map_icons);
            return map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(
                            sportMapIcons.getResourceId(sportId, 0)))
                    .position(location)
                    .title(title)
                    .snippet(snippet));
        } else {
            return null;
        }
    }

    @Override
    public void onUpdate(Location location) {
        LocationDataSource.unSubscribe(this);
        lastTarget = new LatLng(location.getLatitude(), location.getLongitude());
        presenter.findExercises(lastTarget, getActivity(), map);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        presenter.openExercise(marker, getActivity());
    }

    @Override
    public boolean onMyLocationButtonClick() {
        LocationDataSource.subscribe(this);
        return false;
    }
}
