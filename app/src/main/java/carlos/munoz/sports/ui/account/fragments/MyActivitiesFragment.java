package carlos.munoz.sports.ui.account.fragments;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import carlos.munoz.sports.adapter.ExerciseAdapter;
import carlos.munoz.sports.common.BaseUi;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.ui.account.presenter.MyActivitiesPresenter;
import carlos.munoz.sports.ui.account.presenter.MyActivitiesPresenterImpl;
import carlos.munoz.sports.utils.Utils;

public class MyActivitiesFragment extends ListFragment implements MyActivitiesUi {

    private static final String ARG_EXERCISE_LIST_TYPE = "exercise_list_type";

    public static final int VALUE_EXERCISE_LIST_CREATED = 1;
    public static final int VALUE_EXERCISE_LIST_PARTICIPANT = 2;
    public static final int VALUE_EXERCISE_LIST_NEAR_YOU = 3;

    private int mExerciseListType;

    List<Exercise> exercises;

    public static MyActivitiesFragment newInstance(int exerciseListType) {
        MyActivitiesFragment fragment = new MyActivitiesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_EXERCISE_LIST_TYPE, exerciseListType);
        fragment.setArguments(args);
        return fragment;
    }

    public MyActivitiesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mExerciseListType = getArguments().getInt(ARG_EXERCISE_LIST_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MyActivitiesPresenter presenter =
                new MyActivitiesPresenterImpl((BaseUi) getActivity(), this);

        if (savedInstanceState == null) {
            Log.e(Utils.LOG_DEBUG_TAG, "not instanced");
            switch (mExerciseListType) {
                case VALUE_EXERCISE_LIST_CREATED:
                    presenter.getCreatedExercises();
                    break;
                case VALUE_EXERCISE_LIST_PARTICIPANT:
                    presenter.getParticipantExercises();
                    break;
                case VALUE_EXERCISE_LIST_NEAR_YOU:
                    presenter.getNearYouExercises();
            }

        } else {
            Log.e(Utils.LOG_DEBUG_TAG, "already instanced");
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Exercise currentExercise = exercises.get(position);
        Navigator.getInstance().goToViewExercise(currentExercise, getActivity());
    }

    @Override
    public void fillList(List<Exercise> data, String emptyText) {
        exercises = data;
        ExerciseAdapter adapter = new ExerciseAdapter(getActivity(), data);
        setListAdapter(adapter);
        setEmptyText(emptyText);
    }


}
