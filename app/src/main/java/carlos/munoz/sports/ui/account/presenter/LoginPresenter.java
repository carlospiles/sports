package carlos.munoz.sports.ui.account.presenter;

import android.app.Activity;
import android.content.Context;

public interface LoginPresenter {

    public void doLogin(String userName, String password, Context context);

    public void doFacebookLogin(Activity returnActivity);

    public void doSignUp(String userName, String email, String password);
}
