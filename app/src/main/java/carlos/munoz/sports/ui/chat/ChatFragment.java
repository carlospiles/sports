package carlos.munoz.sports.ui.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import carlos.munoz.sports.R;
import carlos.munoz.sports.adapter.MessageAdapter;
import carlos.munoz.sports.entities.Message;
import carlos.munoz.sports.service.ChatService;
import carlos.munoz.sports.utils.Utils;


/**
 * A chat fragment containing messages view and input form.
 */
public class ChatFragment extends Fragment {


    private RecyclerView mMessagesView;
    private EditText mInputMessageView;
    private List<Message> mMessages = new ArrayList<Message>();
    private RecyclerView.Adapter mAdapter;
    private String mRoomId;

    IntentFilter myFilter;
    ResponseReceiver myReceiver;

    public ChatFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(Utils.LOG_DEBUG_TAG, "INSTANCED BROADCAST RECEIVER");
        myReceiver = new ResponseReceiver();
        getActivity().registerReceiver(myReceiver, new IntentFilter(ChatService.BROADCAST_NEW_MESSAGE));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRoomId = ((ChatActivity) getActivity()).roomId;

        mMessagesView = (RecyclerView) view.findViewById(R.id.messages);
        mMessagesView.setLayoutManager(new LinearLayoutManager(getActivity()));
        initMessages();

        mInputMessageView = (EditText) view.findViewById(R.id.message_input);
        mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    attemptSend();
                    return true;
                }
                return false;
            }
        });

        ImageButton sendButton = (ImageButton) view.findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void attemptSend() {

        String message = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            mInputMessageView.requestFocus();
            return;
        }

        mInputMessageView.setText("");

        //send message to service
        Intent serviceIntent = new Intent(getActivity(), ChatService.class);
        serviceIntent.putExtra(ChatService.KEY_ACTION, ChatService.ACTION_SEND);
        serviceIntent.putExtra("message", message);
        serviceIntent.putExtra("room", mRoomId);
        getActivity().startService(serviceIntent);

        //addMessage(mUsername, message);
    }

    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void initMessages() {
        mMessages = Message.find(Message.class, "room_id = ?", mRoomId);
        mAdapter = new MessageAdapter(getActivity(), mMessages);
        mMessagesView.setAdapter(mAdapter);
        scrollToBottom();

    }

    private class ResponseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(Utils.LOG_DEBUG_TAG, "nuevo mensaje recibido en activity por broadcast");
            String roomId = intent.getExtras().getString(ChatService.BROADCAST_EXTRA_ROOM);
            if (roomId.equals(mRoomId)) {
                Log.e(Utils.LOG_DEBUG_TAG, "nuevo mensaje a esta room" + roomId);
                initMessages();
            }else{
                Log.e(Utils.LOG_DEBUG_TAG, "nuevo mensaje a room distinta" + roomId + " VS " + mRoomId);
            }
        }
    }
}
