package carlos.munoz.sports.ui.newexercise;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.gms.maps.model.LatLng;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carlos.munoz.sports.R;
import carlos.munoz.sports.adapter.SportAdapter;
import carlos.munoz.sports.common.BaseActivity;
import carlos.munoz.sports.dialogs.WeekdaysDialog;
import carlos.munoz.sports.entities.Sport;
import carlos.munoz.sports.listener.WeekdaysChangedListener;
import carlos.munoz.sports.ui.newexercise.presenter.NewExercisePresenter;
import carlos.munoz.sports.ui.newexercise.presenter.NewExercisePresenterImpl;
import carlos.munoz.sports.utils.Utils;

public class NewExerciseActivity extends BaseActivity
        implements TimePickerDialog.OnTimeSetListener, WeekdaysChangedListener {

    public static final String TIMEPICKER_TAG_START = "timepicker_start";
    public static final String TIMEPICKER_TAG_END = "timepicker_end";

    private final int MAP_PICKER_CODE = 20;

    final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    ProgressDialog mProgressDialog;

    final Calendar calendar = Calendar.getInstance();
    final TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this,
            calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true, true);

    EditText mTitle;
    EditText mDesc;
    Spinner mSportSpinner;
    RadioGroup mLevelRadio;
    Button mStartDateBtn, mEndDateBtn, mLocationBtn, mWeekdaysBtn;
    NewExercisePresenter presenter;

    LatLng mSelectedlocation = null;
    Date mStartDate = null;
    Date mEndDate = null;
    ArrayList<String> mWeekdays = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new NewExercisePresenterImpl(this, this);

        setupToolbar();

        mTitle = (EditText) findViewById(R.id.title_edittext);
        mDesc = (EditText) findViewById(R.id.desc_edittext);
        mSportSpinner = (Spinner) findViewById(R.id.sport_spinner);
        mLevelRadio = (RadioGroup) findViewById(R.id.level_radiogroup);
        mLocationBtn = (Button) findViewById(R.id.button_location);
        mStartDateBtn = (Button) findViewById(R.id.button_start_date);
        mEndDateBtn = (Button) findViewById(R.id.button_end_date);
        mWeekdaysBtn = (Button) findViewById(R.id.button_weekdays);


        mLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewExerciseActivity.this, MapPickerActivity.class);
                startActivityForResult(i, MAP_PICKER_CODE);
            }
        });

        mStartDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.setCloseOnSingleTapMinute(false);
                timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG_START);
            }
        });

        mEndDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.setCloseOnSingleTapMinute(false);
                timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG_END);
            }
        });

        mWeekdaysBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeekdaysDialog df = WeekdaysDialog.newInstance(getResources()
                        .getStringArray(R.array.weekdays));
                df.show(getFragmentManager(), "DIALOG_WEEKDAYS");
            }
        });

        SportAdapter adapter = new SportAdapter(this,
                Utils.generateSportData(this));
        mSportSpinner.setAdapter(adapter);
    }

    private void setupToolbar() {
        setActionBarIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        setActionBarTitle(getString(R.string.title_activity_new_exercise));
        setActionBarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_exercise;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_exercise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_add_exercise) {
            presenter.createNewExercise(mTitle.getText().toString(), mDesc.getText().toString(),
                    (Sport) mSportSpinner.getSelectedItem(), mLevelRadio.getCheckedRadioButtonId(),
                    mSelectedlocation, mStartDate, mEndDate, mWeekdays);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MAP_PICKER_CODE && resultCode == RESULT_OK) {
            mSelectedlocation = new LatLng(
                    data.getExtras().getDouble("lat"),
                    data.getExtras().getDouble("lon"));
            mLocationBtn.setText("Location (Picked)");
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minute) {
        String tag = timePickerDialog.getTag();
        if (tag.equalsIgnoreCase(TIMEPICKER_TAG_START)) {
            mStartDate = new Date(0, 0, 0, hourOfDay, minute);
            mStartDateBtn.setText("Start time: " + sdf.format(mStartDate));
        } else if (tag.equalsIgnoreCase(TIMEPICKER_TAG_END)) {
            mEndDate = new Date(0, 0, 0, hourOfDay, minute);
            mEndDateBtn.setText("End time: " + sdf.format(mEndDate));
        }
    }

    @Override
    public void onWeekdaysChanged(ArrayList<String> weekdays) {
        if (weekdays.isEmpty()) {
            showToast("You must select at least one week day!");
        } else {
            this.mWeekdays = weekdays;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mWeekdays.size(); i++) {
                sb.append(mWeekdays.get(i));
                if (i == mWeekdays.size() - 2) {
                    sb.append("s and ");
                } else if (i == mWeekdays.size() -1){
                    sb.append("s.");
                } else {
                    sb.append("s, ");
                }
            }
            mWeekdaysBtn.setText(sb.toString());
        }
    }
}
