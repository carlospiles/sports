package carlos.munoz.sports.ui.newexercise.presenter;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Date;

import carlos.munoz.sports.entities.Sport;

public interface NewExercisePresenter {

    public void createNewExercise(String title, String desc, Sport selectedSport,
                                  int selectedLevelResId, LatLng location,
                                  Date startDate, Date endDate, ArrayList<String> weekdays);

}
