package carlos.munoz.sports.ui.home;

import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.model.GeocodingResult;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import carlos.munoz.sports.R;
import carlos.munoz.sports.adapter.AddressAdapter;
import carlos.munoz.sports.common.BaseActivity;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.service.ChatService;
import carlos.munoz.sports.ui.home.fragments.FilterFragment;
import carlos.munoz.sports.ui.home.fragments.MapFragment;
import carlos.munoz.sports.ui.home.presenter.HomePresenter;
import carlos.munoz.sports.ui.home.presenter.HomePresenterImpl;
import carlos.munoz.sports.utils.Utils;

import static android.view.View.OnClickListener;


public class HomeActivity extends BaseActivity implements HomeUi {

    private final int DRAWER_CLICK_MAP = 0;
    private final int DRAWER_CLICK_FITLERS = 1;
    private final int DRAWER_CLICK_ACCOUNT = 2;
    private final int DRAWER_CLICK_LOGOUT = 3;

    private DrawerLayout drawer;

    private MapFragment mapFragment;
    private FilterFragment filterFragment;

    private Menu mMenu;
    private SearchView mSearch;

    HomePresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new HomePresenterImpl(this);

        //TODO borrar
        if(ParseUser.getCurrentUser() != null){
            Intent i = new Intent(this, ChatService.class);
            startService(i);
        }

        setupToolbar();

        presenter.initDrawer(this);

        if (savedInstanceState == null) {
            //just instance a new mapFragment
            mapFragment = new MapFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mapFragment)
                    .commit();
        } else {
            //restore previous fragment instances.
            mapFragment = (MapFragment) getFragmentManager().getFragment(
                    savedInstanceState, "FRAGMENT_MAP");
            filterFragment = (FilterFragment) getFragmentManager().getFragment(
                    savedInstanceState, "FRAGMENT_FILTERS");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setupToolbar() {
        setActionBarIcon(R.drawable.ic_ab_drawer);
        setActionBarTitle(getString(R.string.app_name));
        setActionBarNavigationClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });
    }

    @Override
    public void fillDrawer(String drawerText, String drawerPictureUrl,
                           String[] drawerTitles) {

        //get drawer views
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        ListView drawerList = (ListView) findViewById(R.id.left_drawer_listview);
        TextView drawerUserText = (TextView) findViewById(R.id.left_drawer_textview);
        ImageView drawerImg = (ImageView) findViewById(R.id.left_drawer_image);

        //fill drawer views with data
        drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                drawerTitles));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        drawerUserText.setText(drawerText);

        if (drawerPictureUrl.length() > 0) {
            Picasso.with(this)
                    .load(drawerPictureUrl)
                    .placeholder(R.drawable.profile_placeholder)
                    .resize(72, 72).centerCrop()
                    .into(drawerImg);
        } else {
            Picasso.with(this)
                    .load(R.drawable.profile_placeholder)
                    .resize(72, 72).centerCrop()
                    .into(drawerImg);
        }

    }

    @Override
    public void setSearchSuggestionsAdapter(GeocodingResult[] addresses) {
        if (addresses == null) {
            mSearch.setSuggestionsAdapter(null);
        } else {
            mSearch.setSuggestionsAdapter(new AddressAdapter(HomeActivity.this,
                    Utils.generateCursor(addresses), addresses));
        }
    }

    private void setupSearchWidget() {

        SearchManager mSearchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearch = (SearchView) mMenu.findItem(R.id.action_search).getActionView();
        mSearch.setSearchableInfo(mSearchManager.getSearchableInfo(getComponentName()));
        mSearch.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                GeocodingResult address = ((AddressAdapter) mSearch.getSuggestionsAdapter())
                        .getAddress(position);
                mapFragment.presenter
                        .findExercises(new LatLng(address.geometry.location.lat,
                                address.geometry.location.lng), HomeActivity.this, mapFragment.map);
                return false;
            }
        });
        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.findAddresses(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                presenter.findAddresses(newText);
                return true;
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        this.mMenu = menu;

        setupSearchWidget();

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null != mapFragment) {
            getFragmentManager().putFragment(outState,
                    "FRAGMENT_MAP", mapFragment);
        }
        if (null != filterFragment) {
            getFragmentManager().putFragment(outState,
                    "FRAGMENT_FILTERS", filterFragment);
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {

            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            //fragment replacement
            switch (position) {
                case DRAWER_CLICK_MAP:
                    if (mapFragment == null) {
                        mapFragment = new MapFragment();
                    }
                    transaction.replace(R.id.container, mapFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mMenu.findItem(R.id.action_search).setVisible(true);
                    break;
                case DRAWER_CLICK_FITLERS:
                    if (filterFragment == null) {
                        filterFragment = new FilterFragment();
                    }
                    transaction.replace(R.id.container, filterFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    mMenu.findItem(R.id.action_search).setVisible(false);
                    break;
                case DRAWER_CLICK_ACCOUNT:
                    //navigate to my account or login page depending on login state
                    Navigator.getInstance().goToAccount(HomeActivity.this,
                            ParseUser.getCurrentUser() != null);
                    break;
                case DRAWER_CLICK_LOGOUT:
                    new ParseDataSourceImpl().logOut();
                    Navigator.getInstance().reloadActivity(HomeActivity.this);
            }
            drawer.closeDrawer(Gravity.START);
        }
    }
}
