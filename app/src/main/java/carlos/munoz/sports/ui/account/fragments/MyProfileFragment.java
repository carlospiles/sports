package carlos.munoz.sports.ui.account.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import carlos.munoz.sports.R;
import carlos.munoz.sports.domain.ParseDataSource;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.utils.Utils;

public class MyProfileFragment extends Fragment {

    TextView userNameTv;
    ImageView userPictureTv, userFavSportTv;

    public static MyProfileFragment newInstance(String parseUserId) {
        MyProfileFragment f = new MyProfileFragment();
        Bundle b = new Bundle();
        b.putString("parseUserId", parseUserId);
        f.setArguments(b);
        return f;
    }

    public MyProfileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        Log.e(Utils.LOG_DEBUG_TAG, "entro 1");
        if (savedInstanceState == null) {

            Log.e(Utils.LOG_DEBUG_TAG, "entro 2");
            ParseDataSource p = new ParseDataSourceImpl();

            userNameTv = (TextView) rootView.findViewById(R.id.user_name);
            userPictureTv = (ImageView) rootView.findViewById(R.id.user_photo);
            userFavSportTv = (ImageView) rootView.findViewById(R.id.user_fav_sport);

            p.findUserById(getArguments().getString("parseUserId"), new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    fillUi(parseUser);
                }
            });
        }
        return rootView;
    }

    private void fillUi(ParseUser user) {
        Log.e(Utils.LOG_DEBUG_TAG, user.getUsername() + user.getString("fbId"));
        Picasso.with(getActivity())
                .load(Utils.buildFbPictureUrl(user.getString("fbId")))
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(userPictureTv);
            userNameTv.setText(user.getUsername());
    }


}
