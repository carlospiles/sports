package carlos.munoz.sports.ui.viewexercise;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.List;

import carlos.munoz.sports.R;
import carlos.munoz.sports.adapter.UserListAdapter;
import carlos.munoz.sports.common.BaseActivity;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.ui.chat.ChatActivity;
import carlos.munoz.sports.ui.viewexercise.presenter.ViewExercisePresenterImpl;
import carlos.munoz.sports.utils.Utils;

public class ViewExerciseActivity extends BaseActivity implements ViewExerciseUi {

    ViewExercisePresenterImpl presenter;

    TextView mTitleTv, mDescTv, mDateTv, mSportTv;
    Button mJoinBtn, mChatBtn;
    RecyclerView mUserListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new ViewExercisePresenterImpl(this);

        setupToolbar();

        mTitleTv = (TextView) findViewById(R.id.view_event_title);
        mDescTv = (TextView) findViewById(R.id.view_event_desc);
        mDateTv = (TextView) findViewById(R.id.view_event_date);
        mSportTv = (TextView) findViewById(R.id.view_event_sport);
        mJoinBtn = (Button) findViewById(R.id.view_event_join);
        mChatBtn = (Button) findViewById(R.id.view_event_chat);
        mChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ViewExerciseActivity.this, ChatActivity.class);
                i.putExtra("room", getIntent().getStringExtra(Navigator.INTENT_EXTRA_EXERCISE_ID));
                startActivity(i);
            }
        });
        mUserListView = (RecyclerView) findViewById(R.id.view_event_user_list);
        mUserListView.setLayoutManager(
                new LinearLayoutManager(
                        ViewExerciseActivity.this,
                        LinearLayoutManager.HORIZONTAL,
                        false));


        presenter.findExercise(getIntent().getStringExtra(Navigator.INTENT_EXTRA_EXERCISE_ID));

    }

    @Override
    public void fillUi(Exercise exercise) {

        TypedArray sportIcons = getResources().obtainTypedArray(R.array.sport_icons);
        int sportResId = sportIcons.getResourceId(exercise.getSport(), R.drawable.ic_launcher);


        mTitleTv.setText(exercise.getTitle());
        mDescTv.setText(exercise.getDescription());
        mDateTv.setText(exercise.getPrintableFullDate());
        mSportTv.setCompoundDrawablesWithIntrinsicBounds(sportResId, 0, 0, 0);
        mSportTv.setText(exercise.getPrintableSport(this));

    }

    @Override
    public void switchToUnLoggedUi() {
        mJoinBtn.setText("Join Exercise");
        mChatBtn.setVisibility(View.GONE);
        mJoinBtn.setEnabled(false);
    }

    @Override
    public void switchToJoinedUi() {
        mJoinBtn.setEnabled(true);
        mJoinBtn.setText("Leave Exercise");
        mChatBtn.setVisibility(View.VISIBLE);
        mJoinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.leaveExercise(ViewExerciseActivity.this);
            }
        });
    }

    @Override
    public void switchToUnJoinedUi() {
        mJoinBtn.setEnabled(true);
        mJoinBtn.setText("Join Exercise");
        mChatBtn.setVisibility(View.GONE);
        mJoinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.joinExercise(ViewExerciseActivity.this);
            }
        });
    }

    @Override
    public void fillUserListUi(List<ParseUser> parseUsers) {
        Log.e(Utils.LOG_DEBUG_TAG, "Usuarios suscritos: " + parseUsers.size());
        mUserListView.setHasFixedSize(true);
        final UserListAdapter adapter;
        mUserListView.setAdapter(adapter = new UserListAdapter(parseUsers, R.layout.row_user));
        mUserListView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_view_exercise;
    }

    private void setupToolbar() {
        setActionBarIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        setActionBarTitle(getString(R.string.title_activity_view_exercise));
        setActionBarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_exercise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}
