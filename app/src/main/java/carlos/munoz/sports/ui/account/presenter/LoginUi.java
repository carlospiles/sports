package carlos.munoz.sports.ui.account.presenter;

import carlos.munoz.sports.common.BaseUi;

public interface LoginUi extends BaseUi {

    public void switchToSignUp();

    public void switchToLogin();
}
