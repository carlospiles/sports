package carlos.munoz.sports.ui.chat;

        import android.os.Bundle;
        import android.support.v7.app.ActionBarActivity;

        import carlos.munoz.sports.R;


public class ChatActivity extends ActionBarActivity {

    String roomId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        roomId = getIntent().getStringExtra("room");
        setContentView(R.layout.activity_chat);
    }
}
