package carlos.munoz.sports.ui.account.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.domain.ParseDataSource;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.listener.FacebookLoginListener;
import carlos.munoz.sports.utils.Utils;


public class LoginPresenterImpl implements LoginPresenter {

    LoginUi ui;
    ParseDataSource datasource;

    public LoginPresenterImpl(LoginUi ui) {
        this.ui = ui;
        this.datasource = new ParseDataSourceImpl();
    }

    public void doLogin(String userName, String password, final Context context) {
        if (userName.isEmpty() || password.isEmpty()) {
            ui.showToast("userName and password are required");
            return;
        }
        ui.showProgressDialog("Login", "Trying to log in, wait please");
        datasource.logIn(userName, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e == null) {
                    ui.showToast("Login success!");
                    Navigator.getInstance().goToMain(context);
                } else {
                    ui.showToast(e.getMessage());
                }
                ui.hideProgressDialog();
            }
        });
    }

    @Override
    public void doFacebookLogin(final Activity returnActivity) {
        ui.showProgressDialog("Facebook login", "Trying to log in with Facebook, wait please");
        datasource.facebookLogIn(returnActivity, new FacebookLoginListener() {
            @Override
            public void onSuccess() {
                ui.hideProgressDialog();
                ui.showToast("Login success!");
                Navigator.getInstance().goToMain(returnActivity);
            }

            @Override
            public void onError(String errorMsg) {
                Log.e(Utils.LOG_DEBUG_TAG, errorMsg);
                ui.showToast(errorMsg);
                ui.hideProgressDialog();
            }
        });
    }

    @Override
    public void doSignUp(String userName, String email, String password) {
        if (userName.isEmpty() || password.isEmpty()) {
            ui.showToast("userName, email and password are required");
            return;
        }
        ui.showProgressDialog("Sign Up", "Trying to sign sp, wait please");
        datasource.signUp(userName, email, password, new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ui.showToast("SignUp success! You can now log in");
                    ui.switchToLogin();
                } else {
                    ui.showToast(e.getMessage());
                }
                ui.hideProgressDialog();
            }
        });
    }
}
