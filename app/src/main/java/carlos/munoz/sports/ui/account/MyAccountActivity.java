package carlos.munoz.sports.ui.account;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.parse.ParseUser;

import carlos.munoz.sports.R;
import carlos.munoz.sports.common.BaseActivity;
import carlos.munoz.sports.ui.account.fragments.MyActivitiesFragment;
import carlos.munoz.sports.ui.account.fragments.MyProfileFragment;

public class MyAccountActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupPager();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void setupPager() {
        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_logged_user;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logged_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"Profile", "Created", "Participant"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return MyProfileFragment
                            .newInstance(ParseUser.getCurrentUser().getObjectId());
                case 1:
                    return MyActivitiesFragment
                            .newInstance(MyActivitiesFragment.VALUE_EXERCISE_LIST_CREATED);
                case 2:
                    return MyActivitiesFragment
                            .newInstance(MyActivitiesFragment.VALUE_EXERCISE_LIST_PARTICIPANT);
                default:
                    return null;
            }
        }

    }


}
