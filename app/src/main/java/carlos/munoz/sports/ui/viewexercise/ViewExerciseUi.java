package carlos.munoz.sports.ui.viewexercise;

import com.parse.ParseUser;

import java.util.List;

import carlos.munoz.sports.common.BaseUi;
import carlos.munoz.sports.entities.Exercise;

public interface ViewExerciseUi extends BaseUi {

    public void fillUi(Exercise exercise);

    public void switchToUnLoggedUi();

    public void switchToJoinedUi();

    public void switchToUnJoinedUi();

    public void fillUserListUi(List<ParseUser> parseUsers);
}
