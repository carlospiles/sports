package carlos.munoz.sports.ui.home;


import com.google.maps.model.GeocodingResult;

import carlos.munoz.sports.common.BaseUi;


public interface HomeUi extends BaseUi {

    public void fillDrawer(String drawerText, String drawerPictureUrl, String[] drawerTitles);

    public void setSearchSuggestionsAdapter(GeocodingResult[] addresses);
}
