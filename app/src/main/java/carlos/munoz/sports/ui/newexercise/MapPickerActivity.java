package carlos.munoz.sports.ui.newexercise;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import carlos.munoz.sports.R;
import carlos.munoz.sports.common.BaseActivity;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.domain.LocationDataSource;
import carlos.munoz.sports.listener.LocationListener;

public class MapPickerActivity extends BaseActivity
        implements LocationListener, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMapClickListener {

    ProgressDialog mProgressDialog;

    MapView mapView;
    GoogleMap map;

    private LatLng mSelectedLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mapView = (MapView) findViewById(R.id.mapview_picker);

        //Map init
        mapView.onCreate(savedInstanceState);
        map = mapView.getMap();
        map.setOnMapLoadedCallback(this);
        map.setOnMapClickListener(this);
        MapsInitializer.initialize(this);

        setupToolbar();

    }

    private void setupToolbar() {
        setActionBarIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        setActionBarTitle(getString(R.string.title_activity_map_picker));
        setActionBarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigator.getInstance().goToMain(MapPickerActivity.this);
            }
        });
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationDataSource.unSubscribe(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_map_picker;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_picker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_select_location) {
            if (mSelectedLatLng != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("lat", mSelectedLatLng.latitude);
                returnIntent.putExtra("lon", mSelectedLatLng.longitude);
                setResult(RESULT_OK, returnIntent);
                finish();
            } else {
                showToast("Select location before continue!");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUpdate(Location location) {
        moveMapCamera(location);
        map.clear();
        map.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title("Selected Location!"));
        mSelectedLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        // once location received, unsuscribe from location datasource,
        // making location to update just once.
        LocationDataSource.unSubscribe(this);
    }

    public void moveMapCamera(Location location) {
        if (map != null) {
            // Zoom out to zoom level 10, animating with a duration of 2 seconds.
            map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(17)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onMapLoaded() {
        LocationDataSource.subscribe(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        map.clear();
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Selected Location!"));
        mSelectedLatLng = latLng;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}


