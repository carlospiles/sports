package carlos.munoz.sports.ui.account.presenter;

public interface MyActivitiesPresenter {

    public void getCreatedExercises();

    public void getParticipantExercises();

    public void getNearYouExercises();

    public void openExercise();

    public void deleteExercise();
}
