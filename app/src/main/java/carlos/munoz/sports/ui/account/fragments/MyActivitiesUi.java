package carlos.munoz.sports.ui.account.fragments;

import java.util.List;

import carlos.munoz.sports.entities.Exercise;

public interface MyActivitiesUi {
    public void fillList(List<Exercise> data, String emptyText);
}
