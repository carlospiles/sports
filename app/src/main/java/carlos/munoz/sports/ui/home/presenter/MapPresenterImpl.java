package carlos.munoz.sports.ui.home.presenter;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import carlos.munoz.sports.adapter.ExerciseInfoWindowAdapter;
import carlos.munoz.sports.common.BaseUi;
import carlos.munoz.sports.common.Navigator;
import carlos.munoz.sports.domain.ParseDataSourceImpl;
import carlos.munoz.sports.domain.PreferenceDataSource;
import carlos.munoz.sports.entities.Exercise;
import carlos.munoz.sports.ui.home.fragments.MapUi;

public class MapPresenterImpl implements MapPresenter {

    MapUi ui;
    BaseUi activityUi;
    ParseDataSourceImpl parseDataSource;
    Map<Marker, Exercise> allMarkersMap;

    public MapPresenterImpl(MapUi ui, BaseUi activityUi) {
        this.ui = ui;
        this.parseDataSource = new ParseDataSourceImpl();
        this.activityUi = activityUi;
    }

    @Override
    public void findExercises(final LatLng targetLocation, final Activity context, final GoogleMap map) {
        if(targetLocation == null){
            activityUi.showToast("No ha seleccionado ubicación");
            return;
        }
        activityUi.showProgressDialog("Searching activities", "please wait");
        final PreferenceDataSource preferenceDataSource = new PreferenceDataSource(context);
        parseDataSource.findExercises(targetLocation, preferenceDataSource.getRadius(),
                preferenceDataSource.readFilterExercise(), new FindCallback<Exercise>() {
                    @Override
                    public void done(List<Exercise> exercises, ParseException e) {
                        if (e == null) {
                            allMarkersMap = new HashMap<>();
                            activityUi.showToast(exercises.size() + " FOUND!");
                            ui.moveMapCamera(targetLocation, preferenceDataSource.getRadius());
                            ui.drawMapCircle(targetLocation, preferenceDataSource.getRadius());
                            for (Exercise ex : exercises) {
                                Marker marker = ui
                                        .drawMarker(
                                                ex.getLocation(),
                                                ex.getTitle(),
                                                ex.getDescription(),
                                                ex.getSport()
                                        );
                                allMarkersMap.put(marker, ex);
                            }
                            //set info window adapter
                            map.setInfoWindowAdapter(
                                    new ExerciseInfoWindowAdapter(allMarkersMap, context));

                        } else {
                            activityUi.showToast(e.getMessage());
                        }
                        activityUi.hideProgressDialog();
                    }
                });
    }

    @Override
    public void openExercise(Marker marker, Context context) {
        Exercise ex = allMarkersMap.get(marker);
        Navigator.getInstance().goToViewExercise(ex, context);
    }
}

